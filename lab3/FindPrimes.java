public class FindPrimes{
	public static void main(String[] args){
		int a = Integer.parseInt(args[0]);
		for(int i = 2;i <= a;i++){
			int div = 2;
			boolean isPrime = true;
			while(div < i && isPrime == true){
				if(i % div == 0){
					isPrime = false;
				}
				div++;
			}
			if(isPrime == true){
				System.out.print(i+",");
			}
		}
	}
}
